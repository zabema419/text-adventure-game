const textElement = document.getElementById("text")
const vyberElement = document.getElementById("vyber")

let state = {}

function startGame() {
    state = {}
    showTextNode(1)
}

function showTextNode(textNodeIndex) {
    const textNode = textNodes.find(textNode => textNode.id === textNodeIndex)
    textElement.innerText = textNode.text
    while (vyberElement.firstChild) {
        vyberElement.removeChild(vyberElement.firstChild)
    }

    textNode.options.forEach(option => {
        if (showOption(option)) {
            const button = document.createElement("button")
            button.innerText = option.text
            button.classList.add("tlacitko")
            button.addEventListener("click", () => selectOption(option))
            vyberElement.appendChild(button)
        }
    })
}

function showOption(option) {
    return option.requiredState == null || option.requiredState(state)
}

function selectOption(option) {
    const nextTextNodeId = option.nextText
    if (nextTextNodeId <= 0) {
        return startGame()
    }
    state = Object.assign(state, option.setState)
    showTextNode(nextTextNodeId)
}

const textNodes = [
    {
        id: 1,
        text: "You find yourself in a horse-drawn carriage with four more men. Just like them, you are wearing an armor made of steel and are armed with a sword except yours had fallen on the floor of the carriage. The chief of the group tells you to pick it up and to get ready for action.",
        options: [
            {
                text: "Pick up your sword.",
                setState: { sword: true },
                nextText: 2
            },
            {
                text: "Leave it as it is. You won't need it anyway.",
                nextText: 3
            }
        ]
    },
    {
        id: 2,
        text: "You picked up the sword. The carriage suddenly stops and the chief tells you and the others to follow him.",
        options: [
            {
                text: "Leave the carriage alongside others.",
                nextText: 4
            },
            {
                text: "Stay behind and let them do the work for you.",
                nextText: 5
            }
        ]
    },
    {
        id: 3,
        text: "You angered the chief. However, he has no time to punish you as suddenly the carriage stops. You and the others are instructed to follow the chief.",
        options: [
            {
                text: "Leave the carriage along with others.",
                nextText: 4
            },
            {
                text: "Stay behind and let them do the work for you.",
                nextText: 5
            }
        ]
    },
    {
        id: 4,
        text: "You are in what appears to be an aftermath of what must have been thousands of battles. A wasteland, covered in mud, broken spears, bent swords, cracked shields, pierced pieces of armor, torn banners and disfigured skeletal remains, stretches out as far the eye can see. The dominant of the scene are ruins of what once may have been a fortress. You walk up to the remains of the structure's gate. Behind the passage it forms, you can see a courtyard of the fort, too filled with fallen warriors and their weaponry. But there is more than just the victims of the carnage once unleashed upon this place. A menacing silhouette of an unknown object can be seen in the center of the space towering above the dead. The chief orders two men to follow him and he sends you to secure the outer walls of the fort.",
        options: [
            {
                text: "Head to the eastern walls of the fort.",
                nextText: 6
            },
            {
                text: "Head to the western walls of the fort.",
                nextText: 7
            },
            {
                text: "Disobey and follow the chief to the courtyard.",
                nextText: 8
            }
        ]
    },
    {
        id: 8,
        text: "You silently followed the three men headed for the center of the courtyard. The chief approaches the mysterious object which is now recognizable as a longsword, forged out of dark steel, the core of which is made of what appears to be red tissue emitting menacing red glow and pulsating like a heart.",
        options: [
            {
                text: "Let him take the sword.",
                nextText: 9
            },
            {
                text: "Seize the sword for yourself.",
                nextText: 10
            }
        ]
    },
    {
        id: 10,
        text: "You managed to get ahead of the commander and grabbed the sword's handle. You are consumed by darkness as the weapon's power ovewhelms you and the dark force it contains takes over your body.",
        options: [
            {
                text: "Restart the game",
                nextText: -1
            }
        ]
    },
    {
        id: 9,
        text: "After his fingers closed around the weapon's handle, the chief was overwhelmed by its power and the entity it contained took over his body. Now, you and the two other men are facing what once used to be your superior and what is now a mere host to a dark force.",
        options: [
            {
                text: "Help the soldiers and face the beast.",
                requiredState: (currentState) => currentState.sword,
                nextText: 11
            },
            {
                text: "Flee the courtyard.",
                nextText: 12
            }
        ]
    },
    {
        id: 11,
        text: "You joined the two warriors, facing the abomination, determined to fight it. The beast wields its sword masterfully and slays your two allies with ease. You utilise all the training you were given as a Noxian soldier but your parries are no match for the hard-hitting longsword. You persih just like your fellows.",
        options: [
            {
                text: "Restart the game.",
                nextText: -1
            }
        ]
    },
    {
        id: 12,
        text: "You turned around and started running back to the carriage. You hear the sounds of battle behind you as the others clash with the malevolent being. The ground itself trembles as a part of the fort collapses. Then the shouts of your fellows go silent. You reach the carriage and the horses. They are tied to a dry bush. You turn around to see the two men who were sent to the western walls of the fortress running towards you. Their eyes are filled with fear. They call for your help as the malevolent being is closing in.",
        options: [
            {
                text: "Remain and help them fight the entity.",
                requiredState: (currentState) => currentState.sword,
                nextText: 13
            },
            {
                text: "Untie one of the horses and leave.",
                nextText: 14
            }
        ]
    },
    {
        id: 13,
        text: "You decided to overcome your fear. This decision seals your fate as you, alongside the two other soldiers, prove to be no match for the beast. The longsword crushes your bones and soaks the ground with your blood.",
        options: [
            {
                text: "Restart the game.",
                nextText: -1
            }
        ]
    },
    {
        id: 14,
        text: "You released one of the horses and are now riding it away from the fort as fast as you can. You escaped, at the cost of your dignity and honor, knowing you left your companions to die.",
        options: [
            {
                text: "Restart the game.",
                nextText: -1
            }
        ]
    },
    {
        id: 6,
        text: "Joined by one of your companions, you climbed up a staircase leading to the top of a watchtower that was connected to the walls protecting the fortress from the east. Below you, you see the chief approaching what appears to be a longsword stuck in the ground in the middle of the courtyard. As soon as his fingers close around the weapon's handle, the chief is overwhelmed by its power and the entity it contained takes over his body making him its host. The being faces the two soldiers who were accompanying the chief and slays them with ease. You watch in horror as the monstrosity uses some twisted kind of magic to rip the flesh off their bodies and reshape it to suit its own needs rendering its vessel bigger and stronger.",
        options: [
            {
                text: "Run away.",
                nextText: 16
            },
            {
                text: "Stay and observe the entity.",
                nextText: 15
            }
        ]
    },
    {
        id: 15,
        text: "You kept standing on the spot from which you had whitnessed the bloodshed. The other soldier too seems not to be able to move with fear. Once done with draining the power of its fallen opponents, the being's attention shifts to its surroundings and it notices you. Before you can react, it dashes towards the base of the tower you are standing on and with one swift strike of its mighty longsword shatters it. The structure collapses and you fall towards your death.",
        options: [
            {
                text: "Restart the game.",
                nextText: -1
            }
        ]
    },
    {
        id: 16,
        text: "You ran down the stairs and then headed back to the carriage. You hear the sounds of battle behind you as the others clash with the malevolent being. Then the shouts of your fellows go silent. You reach the carriage and the horses. They are tied to a dry bush. You turn around to see the two men who were sent to the western walls of the fortress running towards you. Their eyes are filled with fear. They call for your help as the malevolent being is closing in.",
        options: [
            {
                text: "Remain and help them fight the entity.",
                requiredState: (currentState) => currentState.sword,
                nextText: 13
            },
            {
                text: "Untie one of the horses and leave.",
                nextText: 14
            }
        ]
    },
    {
        id: 7,
        text: "Alongside one of your fellow soldiers, you ascended the stairs leading to the western walls of the fortress. Below you, you see the chief approaching what appears to be a longsword stuck in the ground in the middle of the courtyard. As soon as his fingers close around the weapon's handle, the chief is overwhelmed by its power and the entity it contained takes over his body making him its host. The being faces the two soldiers who were accompanying the chief and slays them with ease. You watch in horror as the monstrosity uses some twisted kind of magic to rip the flesh off their bodies and reshape it to suit its own needs rendering its vessel bigger and stronger.",
        options: [
            {
                text: "Run away.",
                nextText: 17
            },
            {
                text: "Stay and observe the entity.",
                nextText: 18
            }
        ]
    },
    {
        id: 17,
        text: "You ran down the stairs and then headed back to the carriage. You hear the sounds of battle behind you as the others clash with the malevolent being. The ground itself trembles as a part of the fort collapses. Then the shouts of your fellows go silent. You reach the carriage and the horses. They are tied to a dry bush. You turn around to see the soldier who was sent to the western walls of the fortress running towards you. His eyes are filled with fear. He calls for your help as the malevolent being is closing in.",
        options: [
            {
                text: "Remain and help him fight the entity.",
                requiredState: (currentState) => currentState.sword,
                nextText: 19
            },
            {
                text: "Untie one of the horses and leave.",
                nextText: 14
            }
        ]
    },
    {
        id: 19,
        text: "You decided to overcome your fear. This decision seals your fate as you, alongside the other soldier, prove to be no match for the beast. The longsword crushes your bones and soaks the ground with your blood.",
        options: [
            {
                text: "Restart the game.",
                nextText: -1
            }
        ]
    },
    {
        id: 14,
        text: "You released one of the horses and are now riding it away from the fort as fast as you can. You escaped, at the cost of your dignity and honor, knowing you left your companions to die.",
        options: [
            {
                text: "Restart the game.",
                nextText: -1
            }
        ]
    },
    {
        id: 18,
        text: "Once done with draining the power of its fallen opponents, the being's attention shifted to its surroundings and it noticed the two soldiers who were sent to the eastern walls of the fort and were then standing on top of one of the wathctowers that used to serve as the guard's station points. Before you realize what is happening, the beast crosses the distance between it and the base of the tower and shatters it with one swift strike of its longsword sending the two unfortunate men to their graves. With horror you realize that even the walls beneath you are starting to crumble as the entirity of the structure had lost its integrity. Just like your companions, you fall with the fort and are buried by its rubble.",
        options: [
            {
                text: "Restart the game.",
                nextText: -1
            }
        ]
    },
    {
        id: 5,
        text: "You infuriated the chief with your disobedience. He forces you to follow him and assigns you to clean the latrines once you return to the camp. After following the others, you find yourself in what appears to be an aftermath of what must have been thousands of battles. A wasteland, covered in mud, broken spears, bent swords, cracked shields, pierced pieces of armor, torn banners and disfigured skeletal remains, stretches out as far the eye can see. The dominant of the scene are ruins of what once may have been a fortress. You walk up to the remains of the structure's gate. Behind the passage it forms, you can see a courtyard of the fort, too filled with fallen warriors and their weaponry. But there is more than just the victims of the carnage once unleashed upon this place. A menacing silhouette of an unknown object can be seen in the center of the space towering above the dead. The chief orders two men to follow him and he sends you to secure the outer walls of the fort.",
        options: [
            {
                text: "Head to the eastern walls of the fort.",
                nextText: 6
            },
            {
                text: "Head to the western walls of the fort.",
                nextText: 7
            },
            {
                text: "Disobey and follow the chief to the courtyard.",
                nextText: 8
            }
        ]
    }
]

startGame()